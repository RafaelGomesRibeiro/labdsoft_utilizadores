package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

@RunWith(SpringJUnit4ClassRunner.class)
public class UtilizadorTest {

    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA","a","mail@mail.com");
        user.setCargo(Cargo.DIRETOR);

        UtilizadorDTO usDto = user.toDTO();
        Assertions.assertEquals(user.getCargo().toString(),usDto.cargo);
        Assertions.assertNotEquals(user.getPassword(),usDto.password);
        Assertions.assertEquals(user.getUsername(),usDto.username);

    }

    @Test
    public void testCargo(){
        Utilizador user = new Utilizador("userA","a","mail@mail.com");
        user.setCargo(Cargo.ENFERMEIRO);
        Assertions.assertEquals(Cargo.ENFERMEIRO,user.getCargo());
    }

    @Test
    void fromDtoTest() {
        UtilizadorDTO usDto = new UtilizadorDTO("userA","a", Cargo.ENFERMEIRO.toString());
        usDto.email = "mail";
        Utilizador user = Utilizador.fromDTO(usDto);

        Assertions.assertEquals(user.getEmail(),usDto.email);
        Assertions.assertEquals(user.getPassword(),usDto.password);
        Assertions.assertEquals(user.getUsername(),usDto.username);
    }
}
