package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.PsicologoDTO;

import java.util.ArrayList;

@RunWith(SpringJUnit4ClassRunner.class)
public class PsicologoTest {

    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.PSICOLOGO);
        Psicologo u = new Psicologo(user, "nome", new Agenda(new ArrayList<IntervaloTempo>()));
        PsicologoDTO udto = u.toDTO();
        Assertions.assertEquals(udto.nome, u.nome());

    }


    @Test
    void fromDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.PSICOLOGO);
        Psicologo u;
        Agenda a = new Agenda(new ArrayList<IntervaloTempo>());
        PsicologoDTO udto = new PsicologoDTO(user.toDTO(), "nome", a.toDTO());
        u = Psicologo.fromDTO(udto);
        Assertions.assertEquals(udto.nome, u.nome());

    }
}
