package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;

@RunWith(SpringJUnit4ClassRunner.class)

public class EnfermeiroTest {

    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.ENFERMEIRO);
        Enfermeiro u = new Enfermeiro(user, "nome");
        EnfermeiroDTO udto = u.toDTO();
        Assertions.assertEquals(udto.nome, u.getNome());

    }


    @Test
    void fromDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.ENFERMEIRO);
        Enfermeiro u;
        EnfermeiroDTO udto = new EnfermeiroDTO(user.toDTO(), "nome");
        u = Enfermeiro.fromDTO(udto);
        Assertions.assertEquals(udto.nome, u.getNome());

    }

}
