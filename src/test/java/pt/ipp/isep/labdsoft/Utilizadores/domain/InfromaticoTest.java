package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.InformaticoDTO;

import java.util.ArrayList;

@RunWith(SpringJUnit4ClassRunner.class)

public class InfromaticoTest {

    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.INFORMATICO);
        Informatico u = new Informatico(user, "nome");
        InformaticoDTO udto = u.toDTO();
        Assertions.assertEquals(udto.nome, u.getNome());

    }


    @Test
    void fromDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.INFORMATICO);
        Informatico u;
        InformaticoDTO udto = new InformaticoDTO(user.toDTO(), "nome");
        u = Informatico.fromDTO(udto);
        Assertions.assertEquals(udto.nome, u.getNome());

    }

}
