package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtenteDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

@RunWith(SpringJUnit4ClassRunner.class)
public class UtenteTest {

    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA","a","mail@mail.com");
        user.setCargo(Cargo.UTENTE);
        Utente u = new Utente(1L,"aa",user,EstadoParticipacao.ACEITE);
        UtenteDTO udto = u.toDTO();
        Assertions.assertEquals(udto.numPaciente,u.getNumPaciente());
        Assertions.assertEquals(udto.estado,u.getEstado().toString());
        Assertions.assertEquals(udto.iniciais,u.getIniciais());

    }


    @Test
    void fromDtoTest() {
        Utilizador user = new Utilizador("userA","a","mail@mail.com");
        user.setCargo(Cargo.UTENTE);
        UtenteDTO udto = new UtenteDTO(1L,"aa",user.toDTO(),EstadoParticipacao.ACEITE.toString());
        Utente u = Utente.fromDTO(udto, user);

        Assertions.assertEquals(udto.numPaciente,u.getNumPaciente());
        Assertions.assertEquals(udto.estado,u.getEstado().toString());
        Assertions.assertEquals(udto.iniciais,u.getIniciais());
    }
}
