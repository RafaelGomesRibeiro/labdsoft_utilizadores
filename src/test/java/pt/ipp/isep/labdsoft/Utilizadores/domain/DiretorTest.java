package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.DiretorDTO;

@RunWith(SpringJUnit4ClassRunner.class)

public class DiretorTest {


    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.DIRETOR);
        Diretor u = new Diretor(user, "nome");
        DiretorDTO udto = u.toDTO();
        Assertions.assertEquals(udto.nome, u.getNome());

    }


    @Test
    void fromDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.DIRETOR);
        Diretor u;
        DiretorDTO udto = new DiretorDTO(user.toDTO(), "nome");
        u = Diretor.fromDTO(udto);
        Assertions.assertEquals(udto.nome, u.getNome());

    }
}
