package pt.ipp.isep.labdsoft.Utilizadores.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Utilizadores.dto.MedicoDTO;

import java.util.ArrayList;

@RunWith(SpringJUnit4ClassRunner.class)
public class MedicoTest {

    @Test
    void toDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.MEDICO);
        Medico u = new Medico(user, "nome", new Agenda(new ArrayList<IntervaloTempo>()));
        MedicoDTO udto = u.toDTO();
        Assertions.assertEquals(udto.nome, u.nome());

    }


    @Test
    void fromDtoTest() {
        Utilizador user = new Utilizador("userA", "a", "mail@mail.com");
        user.setCargo(Cargo.MEDICO);
        Medico u;
        Agenda a = new Agenda(new ArrayList<IntervaloTempo>());
        MedicoDTO udto = new MedicoDTO(user.toDTO(), "nome", a.toDTO());
        u = Medico.fromDTO(udto);
        Assertions.assertEquals(udto.nome, u.nome());

    }
}
