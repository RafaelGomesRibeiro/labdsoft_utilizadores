package pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public final class CandidaturaSubmetidaEvent {
    private String requestedAt;
    private String candidaturaId;
    private String utenteId;
    private String utentePassword;
    private String nome;
    private String sobrenome;
    private String email;
}
