package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtenteDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Utente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private Long numPaciente;
    @Column(unique = true)
    private String iniciais;
    @OneToOne(cascade = CascadeType.ALL)
    private Utilizador utilizador;
    @Enumerated(EnumType.STRING)
    private EstadoParticipacao estado;

    public Utente(Long numPaciente, String iniciais, Utilizador utilizador, EstadoParticipacao estado) {

        this.numPaciente = numPaciente;
        this.iniciais = iniciais;
        this.utilizador = utilizador;
        this.estado = estado;

    }

    public static Utente fromDTO(UtenteDTO dto, Utilizador utilizador) {
        if (dto.estado == null || dto.estado.isEmpty()) {
            dto.estado = "PENDENTE";
        }
        utilizador.setCargo(Cargo.UTENTE);
        EstadoParticipacao estado = EstadoParticipacao.valueOf(dto.estado.toUpperCase());
        return new Utente(dto.numPaciente, dto.iniciais, utilizador, estado);
    }

    public UtenteDTO toDTO() {
        String estado = this.estado.toString();
        UtilizadorDTO utilizador = this.utilizador.toDTO();
        return new UtenteDTO(this.numPaciente, this.iniciais, utilizador, estado);
    }

    public void rejeitarParticipacao() {
        estado = EstadoParticipacao.REJEITADO;
    }

    public void aceitarParticipacao() {
        estado = EstadoParticipacao.ACEITE;
    }

}
