package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public final class TokenDTO {
    public String token;
    public String expirationDate;
    public String issuedAt;
}
