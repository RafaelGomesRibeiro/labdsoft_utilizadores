package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.domain.EstadoParticipacao;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtenteDTO;

import java.util.List;
import java.util.Map;


public interface UtenteService {

    List<UtenteDTO> listAll();
    UtenteDTO byId(Long id);
    UtenteDTO byIniciais(String iniciais);
    UtenteDTO createUtente(UtenteDTO dto);
    UtenteDTO atualizarEstadoParticipacao(Long id, Boolean aceite);
    UtenteDTO byIdCartao(Long idCartao);
    UtenteDTO byNumPaciente(Long numPaciente);
    boolean existsUtente(Long numPaciente);
    boolean isAccepted(Long numPaciente);
    Iterable<UtenteDTO> findPendentes();
    Iterable<UtenteDTO> findAceites();
    List<UtenteDTO> utentesComTracker();
    UtenteDTO finalizarParticipação(Long id, EstadoParticipacao estado);
    Map<String, List<Double>> estatisticaMedicaGenes();
    void enviarAlertaUtente(String assunto, String mensagem, String utenteId);
}
