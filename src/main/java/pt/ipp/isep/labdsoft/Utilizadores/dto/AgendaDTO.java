package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AgendaDTO {
    public List<IntervaloTempoDTO> intervalos;
}
