package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Utilizadores.dto.DiretorDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;
import pt.ipp.isep.labdsoft.Utilizadores.services.DiretorService;
import pt.ipp.isep.labdsoft.Utilizadores.services.EnfermeiroService;

import java.util.List;

@RestController
@RequestMapping("diretor")
public class DiretorController {

    @Autowired
    private DiretorService diretorService;

    @GetMapping("/")
    public ResponseEntity<List<DiretorDTO>> enfermeiros() {
        List<DiretorDTO> diretor = diretorService.listAll();
        return new ResponseEntity<List<DiretorDTO>>(diretor, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DiretorDTO> byId(@PathVariable Long id) {
        DiretorDTO diretor = diretorService.byId(id);
        return new ResponseEntity<DiretorDTO>(diretor, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<DiretorDTO> create(@RequestBody DiretorDTO dto) {
        DiretorDTO diretor = diretorService.createDiretor(dto);
        return new ResponseEntity<DiretorDTO>(diretor, HttpStatus.OK);
    }
}
