package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

import java.util.List;

@Repository
public interface PsicologoRepository extends JpaRepository <Psicologo, Long> {

    @Query("SELECT m FROM Psicologo m order by m.agenda.intervalos.size, m.nome")
    public List<Psicologo> selectPsicologoToScheduler();
    Psicologo findPsicologoByUtilizador(Utilizador u);
}
