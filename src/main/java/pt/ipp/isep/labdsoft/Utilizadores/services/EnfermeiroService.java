package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.domain.Enfermeiro;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;

import java.util.List;


public interface EnfermeiroService {

    List<EnfermeiroDTO> listAll();
    EnfermeiroDTO byId(Long id);
    EnfermeiroDTO createEnfermeiro(EnfermeiroDTO dto);

}
