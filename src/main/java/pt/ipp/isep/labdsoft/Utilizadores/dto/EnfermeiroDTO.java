package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class EnfermeiroDTO {
    public UtilizadorDTO utilizador;
    public String nome;
}
