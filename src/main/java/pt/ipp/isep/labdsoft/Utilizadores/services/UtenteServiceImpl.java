package pt.ipp.isep.labdsoft.Utilizadores.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Utilizadores.domain.EstadoParticipacao;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utente;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;
import pt.ipp.isep.labdsoft.Utilizadores.dto.CartaoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.TrackerDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtenteDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.UtenteRepository;
import org.springframework.http.HttpStatus;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UtenteServiceImpl implements UtenteService {

    @Autowired
    private UtenteRepository repo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AES256Encrypter encrypter;

    @Autowired
    private EmailSender emailSender;


    private final WebClient webClient = WebClient.create();

    @Value("${CARTOES_BASE_URL}")
    private String cartoesBaseURL;

    @Value("${TRACKER_BASE_URL}")
    private String trackerBaseURL;

    @Value("${AVALIACOES_BASE_URL}")
    private String avaliacoesBaseURL;

    @Value("${ESTUDO_BASE_URL}")
    private String estudoBaseURL;

    @Override
    public List<UtenteDTO> listAll() {
        return repo.findAll().stream().map(Utente::toDTO).collect(Collectors.toList());

    }

    @Override
    public UtenteDTO byId(Long id) {
        Utente utente = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Utente com id " + id + " não existe"));
        return utente.toDTO();
    }

    @Override
    public UtenteDTO byIniciais(String iniciais) {
        Utente utente = repo.findUtenteByIniciais(iniciais).orElseThrow(() -> new NoSuchElementException("Utente com iniciais " + iniciais + " não existe"));
        return utente.toDTO();
    }

    @Override
    public UtenteDTO createUtente(UtenteDTO dto) {
        final Utilizador utilizador = Utilizador.fromDTO(dto.utilizador);
        utilizador.setPassword(passwordEncoder.encode(utilizador.getPassword()));
        String salt = encrypter.generateSalt();
        utilizador.setEmail(encrypter.encryptString(utilizador.getEmail(), salt));
        utilizador.setSalt(salt);
        Utente utente = Utente.fromDTO(dto, utilizador);
        utente = repo.save(utente);
        return utente.toDTO();
    }

    @Override
    public UtenteDTO atualizarEstadoParticipacao(Long id, Boolean aceite) {
        Utente u = repo.findUtenteByNumPaciente(id).orElseThrow(() -> new NoSuchElementException("Utente com número " + id + " não existe"));
        if (aceite) {
            u.aceitarParticipacao();
        } else {
            u.rejeitarParticipacao();
        }
        u = repo.save(u);
        //TODO e preciso lançar algum evento caso seja aceite ou rejeitado?
        return u.toDTO();
    }

    @Override
    public UtenteDTO finalizarParticipação(Long id, EstadoParticipacao estado) {
        Utente u = repo.findUtenteByNumPaciente(id).orElseThrow(() -> new NoSuchElementException("O Utente não existe"));
        if (u.getEstado() == EstadoParticipacao.ACEITE) {
            if (estado == EstadoParticipacao.CANCELADO || estado == EstadoParticipacao.CONCLUIDO || estado == EstadoParticipacao.DESISTENTE) {
                u.setEstado(estado);
                repo.save(u);
                return u.toDTO();
            } else {
                throw new IllegalArgumentException("O Estudo só pode ser Concluido, Cancelado ou Desistido");
            }
        } else {
            throw new IllegalArgumentException("Esta Operação só pode ser realizada a utentes aceites");
        }
    }

    @Override
    public void enviarAlertaUtente(String assunto, String mensagem, String utenteId) {
        final Utente utente = repo.findUtenteByNumPaciente(Long.parseLong(utenteId)).orElse(null);
        if (utente != null) {
            String emailDescrypted = encrypter.decryptString(utente.getUtilizador().getEmail(), utente.getUtilizador().getSalt());
            emailSender.sendEmail(assunto, mensagem, emailDescrypted);
        }
    }



    @Override
    public UtenteDTO byIdCartao(Long idCartao) {
        CartaoDTO cartaoDTO = webClient.get().uri(cartoesBaseURL + "cartao/" + idCartao).retrieve().onStatus(HttpStatus::is4xxClientError, clientResponse -> {
            return clientResponse.bodyToMono(ErrorDetails.class).flatMap(
                    msg -> {
                        if (msg.getStatusCode() == 404) {
                            throw new NoSuchElementException(msg.getErrorMessage());
                        } else {
                            throw new IllegalArgumentException(msg.getErrorMessage());
                        }
                    }
            );
        }).bodyToMono(CartaoDTO.class).block();
        if (cartaoDTO == null) throw new NoSuchElementException("Cartão não existe");
        if (cartaoDTO.idUtente == null)
            throw new NoSuchElementException("Cartão com o id " + cartaoDTO.idCartao + " não está associado a nenhum utilizador");
        Utente utente = repo.findUtenteByNumPaciente(cartaoDTO.idUtente.longValue()).orElseThrow(() -> new NoSuchElementException("Utente com id " + cartaoDTO.idUtente + " não existe"));
        return utente.toDTO();
    }

    @Override
    public UtenteDTO byNumPaciente(Long numPaciente) {
        Utente utente = repo.findUtenteByNumPaciente(numPaciente).orElseThrow(() -> new NoSuchElementException("Utente com numPaciente " + numPaciente + " não existe"));
        return utente.toDTO();
    }

    @Override
    public boolean existsUtente(Long numPaciente) {
        return repo.existsUtenteByNumPaciente(numPaciente);
    }

    @Override
    public boolean isAccepted(Long numPaciente) {
        return repo.existsUtenteByNumPacienteAndEstado(numPaciente, EstadoParticipacao.ACEITE);
    }

    @Override
    public Iterable<UtenteDTO> findPendentes() {
        return repo.findUtenteByEstado(EstadoParticipacao.PENDENTE).stream().map(Utente::toDTO).collect(Collectors.toList());
    }

    @Override
    public Iterable<UtenteDTO> findAceites() {
        return repo.findUtenteByEstado(EstadoParticipacao.ACEITE).stream().map(Utente::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<UtenteDTO> utentesComTracker() {
        List<TrackerDTO> trackers = webClient.get().uri(trackerBaseURL).retrieve().toEntityList(TrackerDTO.class).block().getBody();
        List<UtenteDTO> utentes = new ArrayList<>();
        for (TrackerDTO tracker : trackers) {
            if (tracker.numPaciente != null)
                utentes.add(byNumPaciente(Long.valueOf(tracker.numPaciente)));
        }
        return utentes;
    }

    @Override
    public Map<String, List<Double>> estatisticaMedicaGenes() {
        Map<String, List<Double>> estatisticaMedica = new LinkedHashMap<>();
        Map<String, List<Integer>> utentesPorTipoGene = webClient.get().uri(avaliacoesBaseURL+"utentesPorTipoGene/").retrieve().toEntity(Map.class).block().getBody();
        //Valores Total Pacientes
        int totalPacientes = 0;
        int totalConcluiuEstudo = 0;
        int totalDesistiuEstudo = 0;
        int totalCancelouEstudo = 0;
        int totalEstudoEmCurso = 0;
        int totalMelhorias = 0;


        for (String gene: utentesPorTipoGene.keySet()) {
            List<Double> list = new ArrayList<>();

            int totalPacientesGene = 0;
            // Analise Estado Utente
            int concluiuEstudo = 0;
            int desistiuEstudo = 0;
            int cancelouEstudo = 0;
            int estudoEmCurso = 0;
            List<Long> utentesConcluiramEstudo = new ArrayList<>();
            for(Integer idAux: utentesPorTipoGene.get(gene)){
                Long id = Long.valueOf(idAux);
                UtenteDTO utente = byNumPaciente(id);
                if(utente.estado.equalsIgnoreCase(EstadoParticipacao.ACEITE.toString())){
                    estudoEmCurso++;
                    totalEstudoEmCurso++;
                } else if (utente.estado.equalsIgnoreCase(EstadoParticipacao.CANCELADO.toString())){
                    cancelouEstudo++;
                    totalCancelouEstudo++;
                }else if (utente.estado.equalsIgnoreCase(EstadoParticipacao.CONCLUIDO.toString())){
                    utentesConcluiramEstudo.add(utente.numPaciente);
                    concluiuEstudo++;
                    totalConcluiuEstudo++;
                }else if (utente.estado.equalsIgnoreCase(EstadoParticipacao.DESISTENTE.toString())){
                    desistiuEstudo++;
                    totalDesistiuEstudo++;
                }
            }
            //Total de Pacientes / Gene
            totalPacientesGene = concluiuEstudo + estudoEmCurso + cancelouEstudo + desistiuEstudo;
            totalPacientes += totalPacientesGene;

            Integer registouMelhoria = 0;
            if(utentesConcluiramEstudo.size()>0){
                registouMelhoria = webClient.post().uri(estudoBaseURL + "consultas/melhorias/").contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(utentesConcluiramEstudo).retrieve().bodyToMono(Integer.class).block();
            }

            totalMelhorias += registouMelhoria;

            list = calculoValores(totalPacientesGene, concluiuEstudo, desistiuEstudo, cancelouEstudo, estudoEmCurso, registouMelhoria);

            estatisticaMedica.put(gene, list);
        }
        List<Double> list = new ArrayList<>();
        list = calculoValores(totalPacientes, totalConcluiuEstudo, totalDesistiuEstudo, totalCancelouEstudo, totalEstudoEmCurso, totalMelhorias);
        estatisticaMedica.put("Total", list);

      return estatisticaMedica;
    }

    private List<Double> calculoValores(int totalPacientesGene,int concluiuEstudo,int desistiuEstudo,int cancelouEstudo,int estudoEmCurso, int registouMelhoria){
        List<Double> list = new ArrayList<>();
        list.add(Double.valueOf(totalPacientesGene));
        Double percentagem = 0.0;
        //Concluiram Estudo / Gene
        list.add(Double.valueOf(concluiuEstudo));
        //Percentagem Concluiu Estudo / Gene
        if(concluiuEstudo == 0 || totalPacientesGene == 0){
            list.add(Double.valueOf(0));
        } else {
            percentagem = round(Double.valueOf(Double.valueOf(concluiuEstudo)/Double.valueOf(totalPacientesGene)) * 100, 2);
            list.add(percentagem);
        }
        //Desistiram Estudo / Gene
        list.add(Double.valueOf(desistiuEstudo));
        //Percentagem Desistiu Estudo / Gene
        if(desistiuEstudo == 0 || totalPacientesGene == 0){
            list.add(Double.valueOf(0));
        } else {
            percentagem = round(Double.valueOf(Double.valueOf(desistiuEstudo)/Double.valueOf(totalPacientesGene)) * 100, 2);
            list.add(percentagem);
        }
        //Cancelou Estudo / Gene
        list.add(Double.valueOf(cancelouEstudo));
        //Percentagem Cancelou Estudo / Gene
        if(cancelouEstudo == 0 || totalPacientesGene == 0){
            list.add(Double.valueOf(0));
        } else {
            percentagem = round(Double.valueOf(Double.valueOf(cancelouEstudo)/Double.valueOf(totalPacientesGene)) * 100, 2);
            list.add(percentagem);
        }
        //Estudo em Curso // Gene
        list.add(Double.valueOf(estudoEmCurso));
        // Percentagem Estudo em Curso // Gene
        if(estudoEmCurso == 0 || totalPacientesGene == 0){
            list.add(Double.valueOf(0));
        } else {
            percentagem = round(Double.valueOf(Double.valueOf(estudoEmCurso)/Double.valueOf(totalPacientesGene)) * 100, 2);
            list.add(percentagem);
        }
        // Concluiram Estudo // Gene (ja foi inserido, mas é usado na segunda parte da tabela)
        list.add(Double.valueOf(concluiuEstudo));
        // Registaram Melhorias // Gene
        list.add(Double.valueOf(registouMelhoria));
        // Percentagem Registou Melhoria // Gene
        if(concluiuEstudo == 0 || registouMelhoria == 0){
            list.add(Double.valueOf(0));
        } else {
            percentagem = round(Double.valueOf(Double.valueOf(registouMelhoria)/Double.valueOf(concluiuEstudo)) * 100, 2);
            list.add(percentagem);
        }
        return list;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
