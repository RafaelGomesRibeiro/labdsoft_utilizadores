package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.Utilizadores.dto.InformaticoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.services.InformaticoService;


import java.util.List;



@RestController
@RequestMapping("informatico")
public final class InformaticoController {

    @Autowired
    private InformaticoService informaticoService;




    @GetMapping("/")
    public ResponseEntity<List<InformaticoDTO>> enfermeiros(){
        List<InformaticoDTO> informaticos = informaticoService.listAll();
        return new ResponseEntity<List<InformaticoDTO>>(informaticos,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<InformaticoDTO> byId(@PathVariable Long id) {
        InformaticoDTO informatico = informaticoService.byId(id);
        return new ResponseEntity<InformaticoDTO>(informatico,HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<InformaticoDTO> create(@RequestBody InformaticoDTO dto){
        InformaticoDTO informatico = informaticoService.createInformatico(dto);
        return new ResponseEntity<InformaticoDTO>(informatico, HttpStatus.OK);
    }

}
