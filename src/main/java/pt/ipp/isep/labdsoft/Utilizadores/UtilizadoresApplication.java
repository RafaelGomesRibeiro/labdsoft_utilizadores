package pt.ipp.isep.labdsoft.Utilizadores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtilizadoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(UtilizadoresApplication.class, args);
	}

}
