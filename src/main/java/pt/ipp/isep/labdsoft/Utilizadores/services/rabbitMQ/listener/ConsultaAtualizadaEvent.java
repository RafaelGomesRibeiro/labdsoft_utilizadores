package pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public final class ConsultaAtualizadaEvent {
    private String inicioAntigo;
    private String fimAntigo;
    private String inicioNovo;
    private String fimNovo;
    private String medicoId;
    private String utenteId;
}
