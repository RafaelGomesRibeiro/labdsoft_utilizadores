package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.dto.InformaticoDTO;

import java.util.List;


public interface InformaticoService {

    List<InformaticoDTO> listAll();
    InformaticoDTO byId(Long id);
    InformaticoDTO createInformatico(InformaticoDTO dto);

}
