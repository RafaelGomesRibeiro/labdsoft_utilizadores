package pt.ipp.isep.labdsoft.Utilizadores.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public final class Tuple<K,V> {
    private K key;
    private V value;
}
