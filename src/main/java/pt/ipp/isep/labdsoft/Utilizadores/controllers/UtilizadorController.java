package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.Utilizadores.dto.TokenDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;
import pt.ipp.isep.labdsoft.Utilizadores.services.UtilizadorService;


import java.util.List;



@RestController
@RequestMapping("utilizador")
public final class UtilizadorController {

    @Autowired
    private UtilizadorService utilizadorService;



    @GetMapping("/")
    public ResponseEntity<List<UtilizadorDTO>> utilizadores(){
        List<UtilizadorDTO> utilizadores = utilizadorService.listAll();
        return new ResponseEntity<List<UtilizadorDTO>>(utilizadores,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UtilizadorDTO> byId(@PathVariable Long id) {
        UtilizadorDTO utilizador = utilizadorService.byId(id);
        return new ResponseEntity<UtilizadorDTO>(utilizador,HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<UtilizadorDTO> authenticate(@RequestBody UtilizadorDTO dto){
        return new ResponseEntity<>(utilizadorService.authenticate(dto), HttpStatus.OK);
    }

    @GetMapping("/exists/{email}")
    public ResponseEntity<Boolean> existsEmail(@PathVariable String email){
        return ResponseEntity.ok(utilizadorService.existsEmail(email));
    }

}
