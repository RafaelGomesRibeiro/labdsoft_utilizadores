package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utente;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

import java.util.Optional;

@Repository
public interface UtilizadorRepository extends JpaRepository<Utilizador, Long> {
    Optional<Utilizador> findUtilizadorByUsername(String username);

    Optional<Utilizador> findUtilizadorByUsernameAndAndPassword(String username, String password);
}
