package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MedicoDTO {
    public UtilizadorDTO utilizador;
    public String nome;
    public AgendaDTO agenda;
}
