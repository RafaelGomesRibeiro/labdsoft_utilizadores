package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.ipp.isep.labdsoft.Utilizadores.dto.AgendaDTO;

import javax.persistence.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class Agenda {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(cascade = CascadeType.ALL)//, orphanRemoval = true)
    @JoinColumn(name = "agenda_id")
    private List<IntervaloTempo> intervalos;

    public Agenda(List<IntervaloTempo> intervalos) {
        this.intervalos = intervalos;
    }

    public int numeroMarcacoes() {
        return intervalos.size();
    }

    public boolean adicionarIntervalo(IntervaloTempo it) {
        return intervalos.add(it);
    }

    public AgendaDTO toDTO() {
        return AgendaDTO.builder().intervalos(intervalos.stream().map(IntervaloTempo::toDTO).collect(Collectors.toList())).build();
    }

    public static Agenda fromDTO(AgendaDTO dto) {
        return new Agenda(dto.intervalos.stream().map(IntervaloTempo::fromDTO).collect(Collectors.toList()));
    }


}
