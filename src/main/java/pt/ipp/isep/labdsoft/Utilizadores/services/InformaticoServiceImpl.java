package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Enfermeiro;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Informatico;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.InformaticoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.EnfermeiroRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.InformaticoRepository;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class InformaticoServiceImpl implements InformaticoService {

    @Autowired
    private InformaticoRepository repo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AES256Encrypter encrypter;

    @Override
    public List<InformaticoDTO> listAll() {
        return repo.findAll().stream().map(Informatico::toDTO).collect(Collectors.toList());
    }

    @Override
    public InformaticoDTO byId(Long id) {
        Informatico informatico = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Informatico com id " + id + " não existe"));
        return informatico.toDTO();
    }

    @Override
    public InformaticoDTO createInformatico(InformaticoDTO dto) {
        dto.utilizador.password = passwordEncoder.encode(dto.utilizador.password);
        Informatico informatico = Informatico.fromDTO(dto);
        String salt = encrypter.generateSalt();
        informatico.getUtilizador().setSalt(salt);
        informatico.getUtilizador().setEmail(encrypter.encryptString(informatico.getUtilizador().getEmail(), salt));
        informatico = repo.save(informatico);
        return informatico.toDTO();
    }

}
