package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Utilizadores.domain.*;
import pt.ipp.isep.labdsoft.Utilizadores.dto.TokenDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.*;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class UtilizadorServiceImpl implements UtilizadorService {

    @Autowired
    private UtilizadorRepository repo;
    @Autowired
    private InformaticoRepository informaticoRepository;
    @Autowired
    private EnfermeiroRepository enfermeiroRepository;
    @Autowired
    private PsicologoRepository psicologoRepository;
    @Autowired
    private MedicoRepository medicoRepository;
    @Autowired
    private DiretorRepository diretorRepository;
    @Autowired
    private UtenteRepository utenteRepository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private AES256Encrypter encrypter;

    @Value("${SECRET_KEY}")
    private String secretKey;

    @Override
    public List<UtilizadorDTO> listAll() {
        return repo.findAll().stream().map(Utilizador::toDTO).collect(Collectors.toList());

    }

    @Override
    public UtilizadorDTO byId(Long id) {
        Utilizador utilizador = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Utilizador com id " + id + " não existe"));
        return utilizador.toDTO();
    }

    @Override
    public UtilizadorDTO createUtilizador(UtilizadorDTO dto) {
        Utilizador utilizador = repo.save(Utilizador.fromDTO(dto));
        return utilizador.toDTO();
    }

    @Override
    public UtilizadorDTO authenticate(UtilizadorDTO dto) {
        Utilizador utilizador = repo.findUtilizadorByUsername(dto.username).orElseThrow(() -> new NoSuchElementException("Credenciais inválidas"));
        if (!encoder.matches(dto.password, utilizador.getPassword()))
            throw new NoSuchElementException("Credenciais inválidas");
        UtilizadorDTO resDto = utilizador.toDTO();
        switch (utilizador.getCargo()) {
            case MEDICO:
                Medico m = medicoRepository.findMedicoByUtilizador(utilizador);
                resDto.id = m.id();
                resDto.nome = m.nome();
                break;
            case UTENTE:
                Utente ut = utenteRepository.findUtenteByUtilizador(utilizador);
                resDto.id = ut.getNumPaciente(); // TODO id utente
                resDto.nome = ""; //TODO mostrar nome do utente na ui?
                break;
            case PSICOLOGO:
                Psicologo p = psicologoRepository.findPsicologoByUtilizador(utilizador);
                resDto.id = p.id();
                resDto.nome = p.nome();
                break;
            case ENFERMEIRO:
                Enfermeiro m1 = enfermeiroRepository.findEnfermeiroByUtilizador(utilizador);
                resDto.id = m1.getId();
                resDto.nome = m1.getNome();
                break;
            case INFORMATICO:
                Informatico inf = informaticoRepository.findInformaticoByUtilizador(utilizador);
                resDto.id = inf.getId();
                resDto.nome = inf.getNome();
                break;
            case DIRETOR:
                Diretor dir = diretorRepository.findDiretorByUtilizador(utilizador);
                resDto.id = dir.getId();
                resDto.nome = dir.getNome();
                break;
            default:
                resDto.nome = "Não encontrado";
                break;
        }
        return resDto;
    }

    @Override
    public Boolean existsEmail(String email) {
        return repo.findAll().stream().anyMatch(u -> encrypter.decryptString(u.getEmail(), u.getSalt()).equalsIgnoreCase(email)); // nao vejas isto ricardo :)
    }
}
