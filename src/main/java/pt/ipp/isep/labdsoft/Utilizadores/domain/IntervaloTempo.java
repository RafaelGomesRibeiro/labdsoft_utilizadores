package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.*;
import lombok.experimental.Accessors;
import pt.ipp.isep.labdsoft.Utilizadores.dto.IntervaloTempoDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Accessors(fluent = true)
public final class IntervaloTempo implements Comparable<IntervaloTempo>{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime inicio;
    private LocalDateTime fim;

    public IntervaloTempo(LocalDateTime inicio, LocalDateTime fim) {
        this.inicio = inicio;
        this.fim = fim;
    }

    public LocalDateTime getFim() {
        return fim;
    }

    public LocalDateTime getInicio() {
        return inicio;
    }

    public IntervaloTempoDTO toDTO() {
        return new IntervaloTempoDTO(inicio, fim);
    }

    public static IntervaloTempo fromDTO(IntervaloTempoDTO dto) {
        return new IntervaloTempo(dto.inicio, dto.fim);
    }

    @Override
    public int compareTo(IntervaloTempo o) {
        if(this.fim == null || o.fim() == null){
            return 0;
        }
        return this.fim.compareTo(o.fim());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntervaloTempo that = (IntervaloTempo) o;

        if (!inicio.equals(that.inicio)) return false;
        return fim.equals(that.fim);
    }

    @Override
    public int hashCode() {
        int result = inicio.hashCode();
        result = 31 * result + fim.hashCode();
        return result;
    }
}
