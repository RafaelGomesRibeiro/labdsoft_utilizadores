package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Utilizador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    private String email;
    @Enumerated(EnumType.STRING)
    private Cargo cargo;
    private String salt;

    public Utilizador(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public static Utilizador fromDTO(UtilizadorDTO dto) {
        return new Utilizador(dto.username, dto.password, dto.email);
    }

    public UtilizadorDTO toDTO() {
        return new UtilizadorDTO(this.username, "", cargo.toString());
    }
}
