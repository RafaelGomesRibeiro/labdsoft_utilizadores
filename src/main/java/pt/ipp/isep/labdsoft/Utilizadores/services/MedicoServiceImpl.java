package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Utilizadores.domain.IntervaloTempo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.dto.MedicoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.IntervaloTempoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.MedicoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.utils.DatesFormatter;


import java.net.URI;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class MedicoServiceImpl implements MedicoService {


    private MedicoRepository repo;

    private PasswordEncoder passwordEncoder;

    private AES256Encrypter encrypter;

    private final WebClient webClient;

    private IntervaloTempoRepository intervaloTempoRepository;

    private UtenteService utenteService;

    private EmailSender emailSender;


    @Value("${GABINETES_BASE_URL}")
    private String gabinetesUrlDisponibilidade;

    @Value("${RESERVAS_BASE_URL}")
    private String reservasUrlDisponibilidade;

    @Value("${AVALIACOES_BASE_URL}")
    private String avaliacoesBaseUrl;


    public MedicoServiceImpl(MedicoRepository repo, PasswordEncoder passwordEncoder, AES256Encrypter encrypter, IntervaloTempoRepository intervaloTempoRepository, UtenteService utenteService, EmailSender emailSender) {
        this.repo = repo;
        this.passwordEncoder = passwordEncoder;
        this.encrypter = encrypter;
        this.webClient = WebClient.create();
        this.intervaloTempoRepository = intervaloTempoRepository;
        this.utenteService = utenteService;
        this.emailSender = emailSender;
    }


    @Override
    public List<MedicoDTO> listAll() {
        return repo.findAll().stream().map(Medico::toDTO).collect(Collectors.toList());
    }

    @Override
    public MedicoDTO byId(Long id) {
        Medico medico = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Médico com id " + id + " não existe"));
        return medico.toDTO();
    }

    @Override
    public MedicoDTO createMedico(MedicoDTO dto) {
        dto.utilizador.password = passwordEncoder.encode(dto.utilizador.password);
        Medico medico = Medico.fromDTO(dto);
        String salt = encrypter.generateSalt();
        medico.utilizador().setSalt(salt);
        medico.utilizador().setEmail(encrypter.encryptString(medico.utilizador().getEmail(), salt));
        medico = repo.save(medico);
        return medico.toDTO();
    }

    @Override
    public Boolean disponibilidade(String data, Long id, Long idUtente) {
        if (!utenteService.isAccepted(idUtente))
            throw new IllegalArgumentException("Utente não existe ou não foi aceite");
        LocalDateTime dataParsed = DatesFormatter.convertToLocalDateTime(data);
        Medico m = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Medico Não existente"));
        for (IntervaloTempo i : m.agenda().getIntervalos()) {
            if (i.inicio().equals(dataParsed))
                throw new IllegalArgumentException("Medico Indisponivel para a Data " + data);
        }
        int numGabinetes = disponibilidadeGabinetes();
        int countAgendas = intervaloTempoRepository.countAllByInicioFromMedico(dataParsed);

        if (numGabinetes <= countAgendas) throw new IllegalArgumentException("Clinica Lotada Para a Data " + data);
        else return disponibilidadeUtente(idUtente, data);

    }

    @Override
    public void novoAgendamento(String medicoId, String dataInicio, String dataFim, String utenteId) {
        Medico m = repo.findById(Long.parseLong(medicoId)).orElseThrow(() -> new NoSuchElementException("Medico Não existente"));
        m.agenda().adicionarIntervalo(IntervaloTempo.builder().inicio(DatesFormatter.convertToLocalDateTime(dataInicio))
                .fim(DatesFormatter.convertToLocalDateTime(dataFim)).build());
        m.agenda().setIntervalos(m.agenda().getIntervalos().stream().sorted(Comparator.comparing(IntervaloTempo::getInicio)).collect(Collectors.toList()));
        repo.save(m);
        CompletableFuture.runAsync(() -> {
            utenteService.enviarAlertaUtente("Consulta marcada", String.format("É com todo o gosto que lhe informamos que a sua consulta com o médico %s das %s às %s foi marcada com sucesso.", m.nome(), dataInicio, dataFim), utenteId);
        });
    }

    @Override
    public void alterarAgendamento(String inicioAntigo, String fimAntigo, String inicioNovo, String fimNovo, String medicoId, String utenteId) {
        Medico m = repo.findById(Long.parseLong(medicoId)).orElseThrow(() -> new NoSuchElementException("Medico Não existente"));
        m.agenda().getIntervalos().remove(IntervaloTempo.builder().inicio(DatesFormatter.convertToLocalDateTime(inicioAntigo)).fim(DatesFormatter.convertToLocalDateTime(fimAntigo)).build());
        m.agenda().adicionarIntervalo(IntervaloTempo.builder().inicio(DatesFormatter.convertToLocalDateTime(inicioNovo))
                .fim(DatesFormatter.convertToLocalDateTime(fimNovo)).build());
        m.agenda().setIntervalos(m.agenda().getIntervalos().stream().sorted(Comparator.comparing(IntervaloTempo::getInicio)).collect(Collectors.toList()));
        repo.save(m);
        CompletableFuture.runAsync(() -> {
            utenteService.enviarAlertaUtente("Consulta alterada", String.format("É com todo o gosto que lhe informamos que a sua consulta com o médico %s das %s às %s foi alterada para a nova data de: %s até às %s.", m.nome(), inicioAntigo, fimAntigo, inicioNovo, fimNovo), utenteId);
        });
    }

    @Override
    public void removerAgendamento(String inicio, String fim, String medicoId, String utenteId) {
        Medico m = repo.findById(Long.parseLong(medicoId)).orElseThrow(() -> new NoSuchElementException("Medico Não existente"));
        m.agenda().getIntervalos().remove(IntervaloTempo.builder().inicio(DatesFormatter.convertToLocalDateTime(inicio)).fim(DatesFormatter.convertToLocalDateTime(fim)).build());
        repo.save(m);
        CompletableFuture.runAsync(() -> {
            utenteService.enviarAlertaUtente("Consulta cancelada", String.format("É com todo o gosto que lhe informamos que a sua consulta com o médico %s das %s às %s foi cancelada.", m.nome(), inicio, fim), utenteId);
        });
    }

    @Override
    public void alertarMedicoLimiteExcedido(String utenteId, String mensagem) {
        Long medicoId = webClient.get().uri(URI.create(String.format("%savaliacaoMedico?utenteId=%s", avaliacoesBaseUrl, utenteId))).retrieve().bodyToMono(Long.class).block();
        Medico medico = repo.findById(medicoId).orElseThrow(() -> new NoSuchElementException("Médico com id " + medicoId + " não existe"));
        String emailDescrypted = encrypter.decryptString(medico.utilizador().getEmail(), medico.utilizador().getSalt());
        emailSender.sendEmail("Limites excedidos", mensagem, emailDescrypted);
    }

    @Override
    public Long findMedico(String convertToLocalDateTime, Long idUtente) {
        List<Medico> meds  = repo.selectMedicoToScheduler();
        for (Medico m: meds) {
            if(this.disponibilidade(convertToLocalDateTime,m.id(),idUtente)) return m.id();
        }

        throw new IllegalArgumentException("Não é possivel Marcar para esta hora");
    }

    private Integer disponibilidadeGabinetes() {
        return webClient.get().uri(gabinetesUrlDisponibilidade + "count").retrieve().bodyToMono(Integer.class).block();
    }

    private Boolean disponibilidadeUtente(Long id, String data) {
        System.out.println("esta e a data: " + data);
        String endpoint = reservasUrlDisponibilidade + String.format("disponibilidadeUtente?id=%s&data=%s", id, data);
        endpoint = endpoint.replaceAll(" ", "%20");
        System.out.println("Este e o endpoint que vou chamar: " + endpoint);
        if (!webClient.get().uri(URI.create(endpoint))
                .retrieve().bodyToMono(Boolean.class).block()) {
            throw new IllegalArgumentException("Utente já tem consulta na hora: " + data);
        }

        return true;
    }

}
