package pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.dispatcher;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class EquipaMedicaAlocadaEvent implements Serializable {
    private String dataInicio;
    private String dataFim;
    private String utenteId;
    private String medicoId;
    private String psicologoId;
}
