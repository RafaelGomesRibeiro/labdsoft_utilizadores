package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Diretor;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.dto.DiretorDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.DiretorRepository;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class DiretorServiceImpl implements DiretorService {


    @Autowired
    private DiretorRepository repo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AES256Encrypter encrypter;
    @Autowired
    private EmailSender emailSender;

    @Override
    public List<DiretorDTO> listAll() {
        return repo.findAll().stream().map(Diretor::toDTO).collect(Collectors.toList());
    }

    @Override
    public DiretorDTO byId(Long id) {
        Diretor diretor = repo.findById(id).orElseThrow(() -> new NoSuchElementException("diretor com id " + id + " não existe"));
        return diretor.toDTO();
    }

    @Override
    public DiretorDTO createDiretor(DiretorDTO dto) {
        dto.utilizador.password = passwordEncoder.encode(dto.utilizador.password);
        Diretor diretor = Diretor.fromDTO(dto);
        String salt = encrypter.generateSalt();
        diretor.getUtilizador().setSalt(salt);
        diretor.getUtilizador().setEmail(encrypter.encryptString(diretor.getUtilizador().getEmail(), salt));
        diretor = repo.save(diretor);
        return diretor.toDTO();
    }

    @Override
    public void envioAlertaTendencias(List<String> alertas) {
        System.out.println("Entrei no diretor service");
        String mensagem = "Foram identificadas as seguintes tendências: ";
        for (String alerta: alertas) {
            mensagem += "\n" + alerta;
        }
        System.out.println("Formatei a mensagem de envio");
        List<Diretor> diretores = repo.findAll();
        System.out.println("Tenho a lista de diretores: " + diretores.size());
        for (Diretor diretor: diretores) {
            System.out.println("Diretor: " + diretor);
            String emailDescrypted = encrypter.decryptString(diretor.getUtilizador().getEmail(), diretor.getUtilizador().getSalt());
            System.out.println("Email Decrypted: " + emailDescrypted);
            emailSender.sendEmail("Tendências Identificadas em Protocolos Médicos", mensagem, emailDescrypted);
            System.out.println("Ja devia ter enviado o email");
        }

    }


}
