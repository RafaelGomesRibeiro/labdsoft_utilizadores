package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.domain.IntervaloTempo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;
import pt.ipp.isep.labdsoft.Utilizadores.utils.Tuple;

public interface Escalonador {
    Agendamento escalonarPreConsulta();
}
