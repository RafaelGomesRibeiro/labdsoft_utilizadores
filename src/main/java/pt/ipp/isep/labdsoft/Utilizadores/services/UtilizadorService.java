package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.dto.TokenDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import java.util.List;


public interface UtilizadorService {

    List<UtilizadorDTO> listAll();
    UtilizadorDTO byId(Long id);
    UtilizadorDTO createUtilizador(UtilizadorDTO dto);
    UtilizadorDTO authenticate(UtilizadorDTO dto);
    Boolean existsEmail(String email);

}
