package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Enfermeiro;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.EnfermeiroRepository;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class EnfermeiroServiceImpl implements EnfermeiroService {

    @Autowired
    private EnfermeiroRepository repo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AES256Encrypter encrypter;

    @Override
    public List<EnfermeiroDTO> listAll() {
        return repo.findAll().stream().map(Enfermeiro::toDTO).collect(Collectors.toList());
    }

    @Override
    public EnfermeiroDTO byId(Long id) {
        Enfermeiro enfermeiro = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Enfermeiro com id " + id + " não existe"));
        return enfermeiro.toDTO();
    }

    @Override
    public EnfermeiroDTO createEnfermeiro(EnfermeiroDTO dto) {
        dto.utilizador.password = passwordEncoder.encode(dto.utilizador.password);
        Enfermeiro enfermeiro = Enfermeiro.fromDTO(dto);
        String salt = encrypter.generateSalt();
        enfermeiro.getUtilizador().setSalt(salt);
        enfermeiro.getUtilizador().setEmail(encrypter.encryptString(enfermeiro.getUtilizador().getEmail(), salt));
        enfermeiro = repo.save(enfermeiro);
        return enfermeiro.toDTO();
    }

}
