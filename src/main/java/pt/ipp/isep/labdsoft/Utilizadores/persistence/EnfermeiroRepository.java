package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Enfermeiro;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

@Repository
public interface EnfermeiroRepository extends JpaRepository <Enfermeiro, Long> {
    Enfermeiro findEnfermeiroByUtilizador(Utilizador u);
}
