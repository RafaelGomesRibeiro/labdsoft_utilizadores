package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.dto.PsicologoDTO;

import java.util.List;

public interface PsicologoService {

    List<PsicologoDTO> listAll();
    PsicologoDTO byId(Long id);
    PsicologoDTO createPsicologo(PsicologoDTO dto);

}
