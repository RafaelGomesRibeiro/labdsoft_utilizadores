package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class TrackerDTO implements Serializable {
    public Integer numPaciente;
    public Integer trackerId;
    public Double tensaoInferior;
    public Double tensaoSuperior;
    public Double temperaturaInferior;
    public Double temperaturaSuperior;
    public Double freqCardiacaInferior;
    public Double freqCardiacaSuperior;
}
