package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import pt.ipp.isep.labdsoft.Utilizadores.dto.AgendaDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.PsicologoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(fluent = true)

public class Psicologo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Utilizador utilizador;
    private String nome;
    @OneToOne(cascade = CascadeType.ALL)
    private Agenda agenda;

    public Psicologo(Utilizador utilizador, String nome, Agenda agenda){
        this.utilizador = utilizador;
        this.nome = nome;
        this.agenda = agenda;
    }

    public static Psicologo fromDTO(PsicologoDTO dto){
        Utilizador utilizador = Utilizador.fromDTO(dto.utilizador);
        utilizador.setCargo(Cargo.PSICOLOGO);
        Agenda agenda = Agenda.fromDTO(dto.agenda);
        return new Psicologo(utilizador, dto.nome, agenda);
    }

    public PsicologoDTO toDTO(){
        UtilizadorDTO utilizador = this.utilizador.toDTO();
        AgendaDTO agendaDto = agenda.toDTO();
        return new PsicologoDTO(utilizador, this.nome, agendaDto);
    }
}