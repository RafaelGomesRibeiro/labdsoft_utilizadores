package pt.ipp.isep.labdsoft.Utilizadores;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Utilizadores.controllers.*;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Cargo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Diretor;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utente;
import pt.ipp.isep.labdsoft.Utilizadores.dto.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Component
public class UtilizadoresBootstrapper implements ApplicationRunner {

    private PsicologoController psicologoController;
    private MedicoController medicoController;
    private InformaticoController informaticoController;
    private EnfermeiroController enfermeiroController;
    private DiretorController diretorController;


    public UtilizadoresBootstrapper(PsicologoController psicologoController, MedicoController medicoController, EnfermeiroController enfermeiroController, InformaticoController informaticoController,DiretorController diretorController) {
        this.psicologoController = psicologoController;
        this.medicoController = medicoController;
        this.informaticoController = informaticoController;
        this.enfermeiroController = enfermeiroController;
        this.diretorController = diretorController;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        registarPsicologos(new String[]{"psi1", "psi2", "psi3", "psi4"}, new String[]{"pass", "pass", "pass", "pass"}, new String[]{"psicologo1", "psicologo2", "psicologo3", "psicologo4"}, new String[]{"labdsoft2020@gmail.com", "labdsoft2020@gmail.com", "labdsoft2020@gmail.com", "labdsoft2020@gmail.com"});
        registarMedicos(new String[]{"med1", "med2", "med3", "med4"}, new String[]{"pass", "pass", "pass", "pass"}, new String[]{"medico1", "medico2", "medico3", "medico4"}, new String[]{"labdsoft2020@gmail.com", "labdsoft2020@gmail.com", "labdsoft2020@gmail.com", "labdsoft2020@gmail.com"});
        registarInformaticos(new String[]{"inf1", "inf2", "inf3"}, new String[]{"pass", "pass", "pass"}, new String[]{"informatico1", "informatico2", "informatico3"}, new String[]{"informatico1@gmail.com", "informatico2@gmail.com", "informatico3@gmail.com"});
        registarEnfermeiros(new String[]{"enf1", "enf2", "enf3"}, new String[]{"pass", "pass", "pass"}, new String[]{"enfermeiro1", "enfermeiro2", "enfermeiro3"}, new String[]{"enfermeiro1@gmail.com", "enfermeiro2@gmail.com", "enfermeiro3@gmail.com"});
        registarDiretor("diretor", "pass", "Diretor", "labdsoft2020@gmail.com");
    }

    private UtilizadorDTO criarUtilizador(String username, String password, String email) {
        UtilizadorDTO dto = new UtilizadorDTO(username, password);
        dto.email = email;
        return dto;
    }

    private AgendaDTO criarAgendaVazia() {
        return AgendaDTO.builder().intervalos(new ArrayList<>()).build();
    }

    private DiretorDTO criarDiretor(String username, String password, String nome, String email) {
        return DiretorDTO.builder().utilizador(criarUtilizador(username, password, email)).nome(nome).build();

    }

    private MedicoDTO criarMedico(String username, String password, String nome, String email) {
        return MedicoDTO.builder().utilizador(criarUtilizador(username, password, email)).nome(nome).agenda(criarAgendaVazia()).build();
    }

    private PsicologoDTO criarPsicologo(String username, String password, String nome, String email) {
        return PsicologoDTO.builder().utilizador(criarUtilizador(username, password, email)).nome(nome).agenda(criarAgendaVazia()).build();
    }

    private InformaticoDTO criarInformatico(String username, String password, String nome, String email) {
        return InformaticoDTO.builder().utilizador(criarUtilizador(username, password, email)).nome(nome).build();
    }

    private EnfermeiroDTO criarEnfermeiro(String username, String password, String nome, String email) {
        return EnfermeiroDTO.builder().utilizador(criarUtilizador(username, password, email)).nome(nome).build();
    }

    private void registarPsicologos(String[] usernames, String[] passwords, String[] nomes, String[] emails) {
        for (int i = 0; i < usernames.length; i++) {
            psicologoController.create(criarPsicologo(usernames[i], passwords[i], nomes[i], emails[i]));
        }
    }

    private void registarMedicos(String[] usernames, String[] passwords, String[] nomes, String[] emails) {
        for (int i = 0; i < usernames.length; i++) {
            medicoController.create(criarMedico(usernames[i], passwords[i], nomes[i], emails[i]));
        }
    }

    private void registarEnfermeiros(String[] usernames, String[] passwords, String[] nomes, String[] emails) {
        for (int i = 0; i < usernames.length; i++) {
            enfermeiroController.create(criarEnfermeiro(usernames[i], passwords[i], nomes[i], emails[i]));
        }
    }

    private void registarDiretor(String username, String password, String nome, String email) {
        diretorController.create(criarDiretor(username, password, nome, email));
    }

    private void registarInformaticos(String[] usernames, String[] passwords, String[] nomes, String[] emails) {
        for (int i = 0; i < usernames.length; i++) {
            informaticoController.create(criarInformatico(usernames[i], passwords[i], nomes[i], emails[i]));
        }
    }
}
