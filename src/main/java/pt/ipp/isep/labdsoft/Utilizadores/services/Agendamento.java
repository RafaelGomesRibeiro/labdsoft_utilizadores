package pt.ipp.isep.labdsoft.Utilizadores.services;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Utilizadores.domain.IntervaloTempo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;

@Getter
@Setter
@Accessors(fluent = true)
@Builder
@AllArgsConstructor
public final class Agendamento {
    private IntervaloTempo intervaloTempo;
    private Medico medico;
    private Psicologo psicologo;
}
