package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.dto.MedicoDTO;

import java.time.LocalDateTime;
import java.util.List;


public interface MedicoService {

    List<MedicoDTO> listAll();
    MedicoDTO byId(Long id);
    MedicoDTO createMedico(MedicoDTO dto);
    Boolean disponibilidade(String data, Long id, Long idUtente);
    void novoAgendamento(String medicoId, String dataInicio, String dataFim, String utenteId);
    void alterarAgendamento(String inicioAntigo, String fimAntigo, String inicioNovo, String fimNovo, String medicoId, String utenteId);
    void removerAgendamento(String inicio, String fim, String medicoId, String utenteId);
    void alertarMedicoLimiteExcedido(String utenteId, String mensagem);

    Long findMedico(String convertToLocalDateTime, Long idUtente);
}
