package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.Utilizadores.domain.EstadoParticipacao;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utente;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtenteDTO;
import pt.ipp.isep.labdsoft.Utilizadores.services.UtenteService;


import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("utente")
public final class UtenteController {

    @Autowired
    private UtenteService utenteService;


    @GetMapping("/")
    public ResponseEntity<List<UtenteDTO>> enfermeiros() {
        List<UtenteDTO> utentes = utenteService.listAll();
        return new ResponseEntity<List<UtenteDTO>>(utentes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UtenteDTO> byId(@PathVariable Long id) {
        UtenteDTO utente = utenteService.byId(id);
        return new ResponseEntity<UtenteDTO>(utente, HttpStatus.OK);
    }


    @GetMapping("/iniciais/{iniciais}")
    public ResponseEntity<UtenteDTO> byIniciais(@PathVariable String iniciais) {
        UtenteDTO utente = utenteService.byIniciais(iniciais);
        return new ResponseEntity<UtenteDTO>(utente, HttpStatus.OK);
    }

    @GetMapping("/cartao/{idCartao}")
    public ResponseEntity<UtenteDTO> byIdCartao(@PathVariable Long idCartao) {
        UtenteDTO utente = utenteService.byIdCartao(idCartao);
        return new ResponseEntity<UtenteDTO>(utente, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<UtenteDTO> create(@RequestBody UtenteDTO dto) {
        UtenteDTO utente = utenteService.createUtente(dto);
        return new ResponseEntity<UtenteDTO>(utente, HttpStatus.OK);
    }

    @GetMapping("/exists/{id}")
    public ResponseEntity<Boolean> existsUtente(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(utenteService.existsUtente(id));

    }

    @GetMapping("/isAccepted/{numPaciente}")
    public ResponseEntity<Boolean> isAccepted(@PathVariable Long numPaciente) {
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(utenteService.isAccepted(numPaciente));

    }

    @GetMapping("/numPaciente/{numPaciente}")
    public ResponseEntity<UtenteDTO> byNumPaciente(@PathVariable Long numPaciente) {
        UtenteDTO utente = utenteService.byNumPaciente(numPaciente);
        return new ResponseEntity<UtenteDTO>(utente, HttpStatus.OK);
    }

    @GetMapping("/pendentes")
    public ResponseEntity<Iterable<UtenteDTO>> pendentes() {
        Iterable<UtenteDTO> utentesPendentes = utenteService.findPendentes();
        return ResponseEntity.ok(utentesPendentes);
    }

    @GetMapping("/aceites")
    public ResponseEntity<Iterable<UtenteDTO>> aceites() {
        Iterable<UtenteDTO> utentesPendentes = utenteService.findAceites();
        return ResponseEntity.ok(utentesPendentes);
    }

    @GetMapping("/utentesComTracker")
    public ResponseEntity<List<UtenteDTO>> utentesComTracker(){
        List<UtenteDTO> utentes = utenteService.utentesComTracker();
        return new ResponseEntity<>(utentes,HttpStatus.OK);
    }

    @PutMapping("/alterarEstado/{numPaciente}")
    public ResponseEntity<UtenteDTO> alterarEstadoUtente(@PathVariable Long numPaciente,@RequestBody EstadoParticipacao estadoParticipacao){
        return new ResponseEntity<>(utenteService.finalizarParticipação(numPaciente, estadoParticipacao),HttpStatus.OK);
    }

    @GetMapping("/estatisticaMedica")
    public ResponseEntity<Map<String, List<Double>>> estatisticaMedica(){
        return ResponseEntity.ok(utenteService.estatisticaMedicaGenes());
    }
}
