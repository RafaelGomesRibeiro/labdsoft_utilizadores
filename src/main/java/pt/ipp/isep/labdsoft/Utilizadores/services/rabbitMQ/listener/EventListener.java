package pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.listener;

import lombok.extern.java.Log;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Utilizadores.domain.*;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.MedicoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.PsicologoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.UtenteRepository;
import pt.ipp.isep.labdsoft.Utilizadores.services.*;
import pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.dispatcher.EquipaMedicaAlocadaEvent;
import pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.Utilizadores.utils.DatesFormatter;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.stream.Collectors;

@Component
@Transactional //In order to fetch the values from lazy fetch
@Log
public class EventListener {

    private static final String CANDIDATURAS_EXCHANGE_NAME = "candidaturas";
    private static final String EXCHANGE_TYPE = "direct";
    private static final String CANDIDATURA_SUBMITTED_KEY = "A";
    private static final String PARECER_MEDICO_SUBMETIDO_KEY = "B";

    private static final String ESTUDO_EXCHANGE_NAME = "estudo";
    private static final String CONSULTA_MARCADA_EVENT_KEY = "A";
    private static final String CONSULTA_ATUALIZADA_EVENT_KEY = "B";
    private static final String CONSULTA_CANCELADA_EVENT_KEY = "C";

    private static final String TRACKERS_EXCHANGE_NAME = "trackers";
    private static final String UTENTE_EXCEDEU_LIMIT_EVENT_KEY = "A";
    private static final String TENDENCIAS_IDENTIFICADAS_EVENT_KEY = "B";



    private UtenteRepository utenteRepository;
    private MedicoRepository medicoRepository;
    private PsicologoRepository psicologoRepository;
    private Escalonador escalonador;
    private EventDispatcher eventDispatcher;
    private UtenteService utenteService;
    private PasswordEncoder passwordEncoder;
    private AES256Encrypter encrypter;
    private MedicoService medicoService;
    private DiretorService diretorService;

    public EventListener(UtenteRepository utenteRepository, Escalonador escalonador, MedicoRepository medicoRepository,
                         PsicologoRepository psicologoRepository, EventDispatcher eventDispatcher, UtenteService utenteService,
                         PasswordEncoder passwordEncoder, AES256Encrypter encrypter, MedicoService medicoService, DiretorService diretorService) {
        this.utenteRepository = utenteRepository;
        this.escalonador = escalonador;
        this.medicoRepository = medicoRepository;
        this.psicologoRepository = psicologoRepository;
        this.eventDispatcher = eventDispatcher;
        this.utenteService = utenteService;
        this.passwordEncoder = passwordEncoder;
        this.encrypter = encrypter;
        this.medicoService = medicoService;
        this.diretorService = diretorService;
    }


    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = CANDIDATURAS_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = CANDIDATURA_SUBMITTED_KEY)})
    public void listenCandidaturaSubmetidaEvent(CandidaturaSubmetidaEvent event) {
        handleCandidaturaSubmetidaEvent(event);
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = CANDIDATURAS_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = PARECER_MEDICO_SUBMETIDO_KEY)})
    public void listenParecerMedicoSubmetidoEvent(ParecerMedicoSubmetidoEvent event) {
        utenteService.atualizarEstadoParticipacao(Long.parseLong(event.getUtenteId()), event.getParecer());
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = ESTUDO_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = CONSULTA_MARCADA_EVENT_KEY)})
    public void listenConsultaMarcadaEvent(ConsultaMarcadaEvent event) {
        medicoService.novoAgendamento(event.getMedicoId(), event.getInicio(), event.getFim(), event.getUtenteId());
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = ESTUDO_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = CONSULTA_ATUALIZADA_EVENT_KEY)})
    public void listenConsultaAtualizadaEvent(ConsultaAtualizadaEvent event) {
        medicoService.alterarAgendamento(event.getInicioAntigo(), event.getFimAntigo(), event.getInicioNovo(), event.getFimNovo(), event.getMedicoId(), event.getUtenteId());
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = ESTUDO_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = CONSULTA_CANCELADA_EVENT_KEY)})
    public void listenConsultaCanceladaEvent(ConsultaCanceladaEvent event) {
        medicoService.removerAgendamento(event.getInicio(), event.getFim(), event.getMedicoId(), event.getUtenteId());
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = TRACKERS_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = UTENTE_EXCEDEU_LIMIT_EVENT_KEY)})
    public void listenUtenteExcedeuLimiteEvent(UtenteExcedeuLimiteEvent event) {
        medicoService.alertarMedicoLimiteExcedido(event.getUtenteId(), event.getMensagem());
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = TRACKERS_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = TENDENCIAS_IDENTIFICADAS_EVENT_KEY)})
    public void listenTendenciasIdentificadasEvent(TendenciasIdentificadasEvent event) {
        System.out.println("Recebi o evento");
        diretorService.envioAlertaTendencias(event.getTendenciasIdentificadas());
    }

    private void handleCandidaturaSubmetidaEvent(CandidaturaSubmetidaEvent event) {
        try {
            String salt = encrypter.generateSalt();
            //TODO refactor desde codigo para um serviço
            Utilizador ut = Utilizador.builder().username(event.getUtenteId()).password(passwordEncoder.encode(event.getUtentePassword()))
                    .email(encrypter.encryptString(event.getEmail(), salt)).salt(salt).cargo(Cargo.UTENTE).build();
            final Utente u = Utente.builder().utilizador(ut).estado(EstadoParticipacao.PENDENTE).iniciais(String.valueOf(event.getNome().charAt(0)) + event.getSobrenome().charAt(0))
                    .numPaciente(Long.parseLong(event.getCandidaturaId())).build();
            int i = 1;
            String iniciaisOriginais = u.getIniciais();
            while (utenteRepository.findAll().stream().anyMatch(utente -> utente.getIniciais().equalsIgnoreCase(u.getIniciais()))) {
                u.setIniciais(iniciaisOriginais + i);
                i++;
            }
            utenteRepository.save(u);
            utenteService.enviarAlertaUtente("Registado com sucesso", String.format("É com enorme prazer que a INOVCLI fornece as suas credenciais de acesso: username=%d e password=%s", u.getNumPaciente(), event.getUtentePassword()),
                    event.getCandidaturaId());
            // escalonar o medico e psicologo e atualizar os seus horarios
            Agendamento agendamento = escalonador.escalonarPreConsulta();

            Medico m = agendamento.medico();
            Psicologo p = agendamento.psicologo();
            m.agenda().adicionarIntervalo(agendamento.intervaloTempo());
            p.agenda().adicionarIntervalo(agendamento.intervaloTempo());
            m.agenda().setIntervalos(m.agenda().getIntervalos().stream().sorted(Comparator.comparing(IntervaloTempo::getInicio)).collect(Collectors.toList()));
            p.agenda().setIntervalos(p.agenda().getIntervalos().stream().sorted(Comparator.comparing(IntervaloTempo::getInicio)).collect(Collectors.toList()));


            medicoRepository.save(m);
            psicologoRepository.save(p);
            // lancar evento de equipa medica alocada
            EquipaMedicaAlocadaEvent e = EquipaMedicaAlocadaEvent.builder().dataInicio(DatesFormatter.convertToString(agendamento.intervaloTempo().inicio()))
                    .dataFim(DatesFormatter.convertToString(agendamento.intervaloTempo().fim())).medicoId(String.valueOf(m.id()))
                    .psicologoId(String.valueOf(p.id())).utenteId(String.valueOf(u.getNumPaciente())).build();
            eventDispatcher.dispatchEquipaMedicaAlocadaEvent(e);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("----------");
            System.out.println(e.getStackTrace().toString());
            log.severe(e.getMessage());
        }
    }

}
