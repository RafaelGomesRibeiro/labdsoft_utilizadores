package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Enfermeiro;
import pt.ipp.isep.labdsoft.Utilizadores.domain.EstadoParticipacao;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utente;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface UtenteRepository extends JpaRepository <Utente, Long> {
    public Optional<Utente> findUtenteByIniciais(String iniciais);
    public Optional<Utente> findUtenteByNumPaciente(Long numPaciente);
    public Boolean existsUtenteByNumPaciente(Long numPaciente);
    Boolean existsUtenteByNumPacienteAndEstado(Long numPaciente, EstadoParticipacao estadoParticipacao);
    public Collection<Utente> findUtenteByEstado(EstadoParticipacao estadoParticipacao);
    Utente findUtenteByUtilizador(Utilizador u);
}
