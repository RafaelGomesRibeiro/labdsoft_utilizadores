package pt.ipp.isep.labdsoft.Utilizadores.dto;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class CartaoDTO {
    public Integer idCartao;
    public Integer idUtente;
}
