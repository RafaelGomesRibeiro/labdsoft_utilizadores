package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;
import pt.ipp.isep.labdsoft.Utilizadores.services.EnfermeiroService;


import java.util.List;


@RestController
@RequestMapping("enfermeiro")
public final class EnfermeiroController {

    @Autowired
    private EnfermeiroService enfermeiroService;

    @GetMapping("/")
    public ResponseEntity<List<EnfermeiroDTO>> enfermeiros() {
        List<EnfermeiroDTO> enfermeiros = enfermeiroService.listAll();
        return new ResponseEntity<List<EnfermeiroDTO>>(enfermeiros, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EnfermeiroDTO> byId(@PathVariable Long id) {
        EnfermeiroDTO enfermeiro = enfermeiroService.byId(id);
        return new ResponseEntity<EnfermeiroDTO>(enfermeiro, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<EnfermeiroDTO> create(@RequestBody EnfermeiroDTO dto) {
        EnfermeiroDTO enfermeiro = enfermeiroService.createEnfermeiro(dto);
        return new ResponseEntity<EnfermeiroDTO>(enfermeiro, HttpStatus.OK);
    }

}
