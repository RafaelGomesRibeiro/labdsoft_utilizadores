package pt.ipp.isep.labdsoft.Utilizadores.domain;

public enum EstadoParticipacao {
    ACEITE {
        @Override
        public String toString() {
            return "Aceite";
        }
    },
    REJEITADO {
        @Override
        public String toString() {
            return "Rejeitado";
        }
    },
    PENDENTE {
        @Override
        public String toString() {
            return "Pendente";
        }
    },
    CANCELADO {
        @Override
        public String toString() {
            return "Cancelado";
        }
    },
    DESISTENTE {
        @Override
        public String toString() {
            return "Desistente";
        }
    },
    CONCLUIDO {
        @Override
        public String toString() {
            return "Concluido";
        }
    }
}
