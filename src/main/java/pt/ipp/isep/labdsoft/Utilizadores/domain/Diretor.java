package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Utilizadores.dto.DiretorDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Diretor {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Utilizador utilizador;
    private String nome;

    public Diretor(Utilizador utilizador, String nome){
        this.utilizador = utilizador;
        this.nome = nome;
    }

    public static Diretor fromDTO(DiretorDTO dto){
        Utilizador utilizador = Utilizador.fromDTO(dto.utilizador);
        utilizador.setCargo(Cargo.DIRETOR);
        return new Diretor(utilizador, dto.nome);
    }

    public DiretorDTO toDTO(){
        UtilizadorDTO utilizador = this.utilizador.toDTO();
        return new DiretorDTO(utilizador, this.nome);
    }
}
