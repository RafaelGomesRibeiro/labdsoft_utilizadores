package pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.dispatcher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public final class EventDispatcher {
    private static final String USERS_EXCHANGE_NAME = "utilizadores";
    private static final String EQUIPA_MEDICA_ALOCADA_EVENT_KEY = "A";

    private RabbitTemplate rabbitTemplate;

    public EventDispatcher(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }

    public void dispatchEquipaMedicaAlocadaEvent(EquipaMedicaAlocadaEvent event){
        rabbitTemplate.convertAndSend(USERS_EXCHANGE_NAME, EQUIPA_MEDICA_ALOCADA_EVENT_KEY, event);
    }
}
