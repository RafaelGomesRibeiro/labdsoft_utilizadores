package pt.ipp.isep.labdsoft.Utilizadores.services;

import lombok.extern.java.Log;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.logging.Level;

@Log
@Component
public final class EmailSender {
    private static final String FROM = "labdsoft2020@gmail.com";

    private JavaMailSender mailSender;

    public EmailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendEmail(String subject, String text, String... sendTo) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(FROM);
            message.setTo(sendTo);
            message.setSubject(subject);
            message.setText(text);
            mailSender.send(message);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }

    }
}
