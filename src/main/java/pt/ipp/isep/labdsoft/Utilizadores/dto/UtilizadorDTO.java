package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class UtilizadorDTO {
    public String username;
    public String password;
    public String cargo;
    public String nome;
    public String email;
    public Long id;

    public UtilizadorDTO(String username, String password, String cargo) {
        this.username = username;
        this.password = password;
        this.cargo = cargo;
    }

    public UtilizadorDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

}
