package pt.ipp.isep.labdsoft.Utilizadores.services;

import pt.ipp.isep.labdsoft.Utilizadores.dto.DiretorDTO;

import java.util.List;

public interface DiretorService {

    List<DiretorDTO> listAll();
    DiretorDTO byId(Long id);
    DiretorDTO createDiretor(DiretorDTO dto);
    void envioAlertaTendencias(List<String> alertas);

}
