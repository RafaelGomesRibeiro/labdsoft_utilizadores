package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Informatico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

@Repository
public interface InformaticoRepository extends JpaRepository <Informatico, Long> {
    Informatico findInformaticoByUtilizador(Utilizador utilizador);
}
