package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Diretor;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Enfermeiro;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

public interface DiretorRepository  extends JpaRepository<Diretor, Long> {
    Diretor findDiretorByUtilizador(Utilizador u);
}