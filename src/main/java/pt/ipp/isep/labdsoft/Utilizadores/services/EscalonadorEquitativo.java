package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Agenda;
import pt.ipp.isep.labdsoft.Utilizadores.domain.IntervaloTempo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.IntervaloTempoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.MedicoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.PsicologoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.utils.Tuple;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EscalonadorEquitativo implements Escalonador {

    private final MedicoRepository medicoRepository;
    private final PsicologoRepository psicologoRepository;
    private final IntervaloTempoRepository intervaloTempoRepository;
    private final WebClient webClient;

    @Value("${GABINETES_BASE_URL}")
    private String gabinetesUrlDisponibilidade;

    // Usar constructor injection em vez de field injection
    public EscalonadorEquitativo(MedicoRepository medicoRepository, PsicologoRepository psicologoRepository, IntervaloTempoRepository intervaloTempoRepository) {
        this.medicoRepository = medicoRepository;
        this.psicologoRepository = psicologoRepository;
        this.webClient = WebClient.create();
        this.intervaloTempoRepository = intervaloTempoRepository;
    }

    @Override
    public Agendamento escalonarPreConsulta() {
        Medico m = medicoRepository.selectMedicoToScheduler().stream().findFirst().orElseThrow(() -> new NoSuchElementException("Não existem médicos")); //TODO testing purposes
        Psicologo p = psicologoRepository.selectPsicologoToScheduler().stream().findFirst().orElseThrow(() -> new NoSuchElementException("Não Existem Psicologos")); //TODO testing purposes
        //TODO MUDAR PARA PROXIMA EM COMUM E NAO PARA A ULTIMA DE CADA
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime defaultDate = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), 9, 0, 0);

        // IntervaloTempo iM = m.agenda().getIntervalos().stream().sorted(Comparator.comparing(IntervaloTempo::getFim).reversed()).findFirst().orElse(IntervaloTempo.builder().inicio(defaultDate.plusDays(3)).fim(defaultDate.plusDays(3).plusMinutes(30)).build());
        // IntervaloTempo iP = p.agenda().getIntervalos().stream().sorted(Comparator.comparing(IntervaloTempo::getFim).reversed()).findFirst().orElse(IntervaloTempo.builder().inicio(defaultDate.plusDays(3)).fim(defaultDate.plusDays(3).plusMinutes(30)).build());
        IntervaloTempo iM = getNextAgendamento(m.agenda(), defaultDate);
        IntervaloTempo iP = getNextAgendamento(p.agenda(), defaultDate);


        Integer limiteConsultas = disponibilidadeGabinetes();

        LocalDateTime inicio;
        if (iM.fim().isAfter(iP.fim())) {
            inicio = iM.fim();
        } else {
            inicio = iP.fim();
        }

        return construirAgendamento(inicio, m, p, limiteConsultas);

    }

    private IntervaloTempo getNextAgendamento(Agenda a, LocalDateTime defaultDate) {


        IntervaloTempo auxI = new IntervaloTempo(LocalDateTime.MIN, LocalDateTime.MIN);
        for (IntervaloTempo i2 : a.getIntervalos()) {
            if (!auxI.fim().equals(i2.inicio()) && !auxI.fim().equals(LocalDateTime.MIN)) {
                return auxI;
            } else {
                auxI = i2;
            }
        }

        return IntervaloTempo.builder().inicio(defaultDate.plusDays(3)).fim(defaultDate.plusDays(3).plusMinutes(30)).build();

    }

    private Agendamento construirAgendamento(LocalDateTime inicio, Medico m, Psicologo p, Integer limiteConsultas) {

        if (inicio.plusMinutes(30).getHour() < 18) {
            return validarAgenda(inicio, m, p, limiteConsultas);
        } else {
            LocalDateTime defaultDate = LocalDateTime.of(inicio.getYear(), inicio.getMonthValue(), inicio.getDayOfMonth() + 1, 9, 0, 0);
            return validarAgenda(defaultDate, m, p, limiteConsultas);
        }
    }

    private Agendamento validarAgenda(LocalDateTime inicio, Medico m, Psicologo p, Integer limiteConsultas) {
        System.out.println(inicio + " INICIO");
        Integer numconsultas = intervaloTempoRepository.countAllByInicioFromMedico(inicio);
        System.out.println(numconsultas + " dDD");
        if (limiteConsultas == numconsultas
                || m.agenda().getIntervalos().stream().anyMatch(intervaloTempo -> intervaloTempo.inicio().equals(inicio))
                || p.agenda().getIntervalos().stream().anyMatch(intervaloTempo -> intervaloTempo.inicio().equals(inicio))) { // || ..|| adicionado agora
            System.out.println("DEB");
            return construirAgendamento(inicio.plusMinutes(30), m, p, limiteConsultas);
        } else {
            return Agendamento.builder().intervaloTempo(IntervaloTempo.builder().inicio(inicio).fim(inicio.plusMinutes(30)).build())
                    .medico(m).psicologo(p).build();
        }
    }


    private Integer disponibilidadeGabinetes() {
        return webClient.get().uri(gabinetesUrlDisponibilidade + "count").retrieve().bodyToMono(Integer.class).block();
    }

}
