package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Agenda;
import pt.ipp.isep.labdsoft.Utilizadores.domain.IntervaloTempo;

import java.time.LocalDateTime;

@Repository
public interface IntervaloTempoRepository extends JpaRepository <IntervaloTempo,Long> {


    public Integer countAllByInicio(LocalDateTime inicio);

    @Query(value = "SELECT COUNT(*) FROM intervalo_tempo it WHERE it.inicio = :inicio AND it.agenda_id IN (SELECT a.id FROM agenda a WHERE a.id in (SELECT u.agenda_id FROM medico u))", nativeQuery = true)
    public Integer countAllByInicioFromMedico(LocalDateTime inicio);

}
