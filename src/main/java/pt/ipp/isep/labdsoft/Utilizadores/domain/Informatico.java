package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pt.ipp.isep.labdsoft.Utilizadores.dto.InformaticoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


public class Informatico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Utilizador utilizador;
    private String nome;

    public Informatico(Utilizador utilizador, String nome){
        this.utilizador = utilizador;
        this.nome = nome;
    }

    public static Informatico fromDTO(InformaticoDTO dto){
        Utilizador utilizador = Utilizador.fromDTO(dto.utilizador);
        utilizador.setCargo(Cargo.INFORMATICO);
        return new Informatico(utilizador, dto.nome);
    }

    public InformaticoDTO toDTO(){
        UtilizadorDTO utilizador = this.utilizador.toDTO();
        return new InformaticoDTO(utilizador, this.nome);
    }
}