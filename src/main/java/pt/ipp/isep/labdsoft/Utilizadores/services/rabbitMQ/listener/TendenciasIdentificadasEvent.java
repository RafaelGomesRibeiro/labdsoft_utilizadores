package pt.ipp.isep.labdsoft.Utilizadores.services.rabbitMQ.listener;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class TendenciasIdentificadasEvent implements Serializable {
    private List<String> tendenciasIdentificadas;
}