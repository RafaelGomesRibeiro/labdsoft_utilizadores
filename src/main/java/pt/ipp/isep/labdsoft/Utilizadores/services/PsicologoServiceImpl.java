package pt.ipp.isep.labdsoft.Utilizadores.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Cargo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;
import pt.ipp.isep.labdsoft.Utilizadores.dto.PsicologoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.PsicologoRepository;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class PsicologoServiceImpl implements PsicologoService {

    @Autowired
    private PsicologoRepository repo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AES256Encrypter encrypter;

    @Override
    public List<PsicologoDTO> listAll() {
        return repo.findAll().stream().map(Psicologo::toDTO).collect(Collectors.toList());

    }

    @Override
    public PsicologoDTO byId(Long id) {
        Psicologo psicologo = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Psicologo com id " + id + " não existe"));
        return psicologo.toDTO();
    }

    @Override
    public PsicologoDTO createPsicologo(PsicologoDTO dto) {
        dto.utilizador.password = passwordEncoder.encode(dto.utilizador.password);
        Psicologo psicologo = Psicologo.fromDTO(dto);
        String salt = encrypter.generateSalt();
        psicologo.utilizador().setSalt(salt);
        psicologo.utilizador().setEmail(encrypter.encryptString(psicologo.utilizador().getEmail(), salt));
        psicologo = repo.save(psicologo);
        return psicologo.toDTO();
    }

}
