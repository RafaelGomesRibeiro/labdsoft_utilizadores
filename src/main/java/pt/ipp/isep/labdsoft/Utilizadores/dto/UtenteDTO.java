package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder

public class UtenteDTO {
    public Long numPaciente;
    public String iniciais;
    public UtilizadorDTO utilizador;
    public String estado;
}
