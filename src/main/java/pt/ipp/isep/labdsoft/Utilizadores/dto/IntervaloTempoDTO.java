package pt.ipp.isep.labdsoft.Utilizadores.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
public class IntervaloTempoDTO {
    public LocalDateTime inicio;
    public LocalDateTime fim;
}
