package pt.ipp.isep.labdsoft.Utilizadores.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Utilizadores.domain.IntervaloTempo;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Utilizador;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Repository
public interface MedicoRepository extends JpaRepository <Medico, Long> {
    @Query("SELECT m FROM Medico m order by m.agenda.intervalos.size, m.nome")
    public List<Medico> selectMedicoToScheduler();

    Medico findMedicoByUtilizador(Utilizador utilizador);
    Optional<Medico> findById(Long id);
}
