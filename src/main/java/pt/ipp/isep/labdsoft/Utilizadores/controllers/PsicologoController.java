package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.Utilizadores.domain.Psicologo;
import pt.ipp.isep.labdsoft.Utilizadores.dto.MedicoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.PsicologoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.services.PsicologoService;


import java.util.List;



@RestController
@RequestMapping("psicologo")
public final class PsicologoController {

    @Autowired
    private PsicologoService psicologoService;



    @GetMapping("/")
    public ResponseEntity<List<PsicologoDTO>> psicologos(){
        List<PsicologoDTO> psicologos = psicologoService.listAll();
        return new ResponseEntity<List<PsicologoDTO>>(psicologos,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PsicologoDTO> byId(@PathVariable Long id) {
        PsicologoDTO psicologo = psicologoService.byId(id);
        return new ResponseEntity<PsicologoDTO>(psicologo,HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<PsicologoDTO> create(@RequestBody PsicologoDTO dto){
        PsicologoDTO psicologo = psicologoService.createPsicologo(dto);
        return new ResponseEntity<PsicologoDTO>(psicologo, HttpStatus.OK);
    }

}
