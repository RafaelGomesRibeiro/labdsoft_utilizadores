package pt.ipp.isep.labdsoft.Utilizadores.domain;

public enum Cargo {
    MEDICO {
        @Override
        public String toString() {
            return "Medico";
        }
    },
    UTENTE {
        @Override
        public String toString() {
            return "Utente";
        }
    },
    ENFERMEIRO {
        @Override
        public String toString() {
            return "Enfermeiro";
        }
    },
    PSICOLOGO {
        @Override
        public String toString() {
            return "Psicologo";
        }
    },
    INFORMATICO {
        @Override
        public String toString() {
            return "Informatico";
        }
    },
    DIRETOR {
        @Override
        public String toString() { return "Diretor"; }
    }
}
