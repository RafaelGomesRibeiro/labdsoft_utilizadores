package pt.ipp.isep.labdsoft.Utilizadores.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pt.ipp.isep.labdsoft.Utilizadores.dto.EnfermeiroDTO;
import pt.ipp.isep.labdsoft.Utilizadores.dto.UtilizadorDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Enfermeiro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Utilizador utilizador;
    private String nome;

    public Enfermeiro(Utilizador utilizador, String nome){
        this.utilizador = utilizador;
        this.nome = nome;
    }

    public static Enfermeiro fromDTO(EnfermeiroDTO dto){
        Utilizador utilizador = Utilizador.fromDTO(dto.utilizador);
        utilizador.setCargo(Cargo.ENFERMEIRO);
        return new Enfermeiro(utilizador, dto.nome);
    }

    public EnfermeiroDTO toDTO(){
        UtilizadorDTO utilizador = this.utilizador.toDTO();
        return new EnfermeiroDTO(utilizador, this.nome);
    }
}