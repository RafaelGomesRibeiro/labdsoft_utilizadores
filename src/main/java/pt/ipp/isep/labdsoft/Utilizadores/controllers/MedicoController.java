package pt.ipp.isep.labdsoft.Utilizadores.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Utilizadores.domain.Medico;
import pt.ipp.isep.labdsoft.Utilizadores.dto.MedicoDTO;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.IntervaloTempoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.persistence.MedicoRepository;
import pt.ipp.isep.labdsoft.Utilizadores.services.MedicoService;
import pt.ipp.isep.labdsoft.Utilizadores.utils.DatesFormatter;


import javax.swing.text.DateFormatter;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("medico")
public final class MedicoController {

    @Autowired
    private MedicoService medicoService;
    @Autowired
    private IntervaloTempoRepository repo;

    @GetMapping("/test")
    public ResponseEntity<Integer> teste(@RequestParam("data") String data){
        return new ResponseEntity<Integer>(repo.countAllByInicioFromMedico(DatesFormatter.convertToLocalDateTime(data)),HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<MedicoDTO>> medicos(){
        List<MedicoDTO> medicos = medicoService.listAll();
        return new ResponseEntity<List<MedicoDTO>>(medicos,HttpStatus.OK);
    }

    @GetMapping("/findMedicoDisponivel")
    public ResponseEntity<Long> medicoDisponivel(@RequestParam("data") String data, @RequestParam("idUtente") Long idUtente){
        return new ResponseEntity<Long>(medicoService.findMedico(data,idUtente),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MedicoDTO> byId(@PathVariable Long id) {
        MedicoDTO medico = medicoService.byId(id);
        return new ResponseEntity<MedicoDTO>(medico,HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<MedicoDTO> create(@RequestBody MedicoDTO dto){
        MedicoDTO medico = medicoService.createMedico(dto);
        return new ResponseEntity<MedicoDTO>(medico, HttpStatus.OK);
    }

    @GetMapping("/disponibilidade")
    public ResponseEntity<Boolean> validarDisponibilidade(@RequestParam("id") Long id, @RequestParam("data") String data, @RequestParam("idUtente") Long idUtente){
        return new ResponseEntity<Boolean>(medicoService.disponibilidade(data,id, idUtente),HttpStatus.OK);
    }



}
